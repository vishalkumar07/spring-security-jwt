package com.authexample.controller;

import com.authexample.model.AuthenticationRequest;
import com.authexample.model.AuthenticationResponse;
import com.authexample.service.JwtUtil;
import com.authexample.service.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailsService userDetailsService;

    @Autowired
    private JwtUtil jwtUtil;

    @GetMapping("/home")
    public String home() {
        return "In Home";
    }

    @PostMapping("/auth")
    public AuthenticationResponse createAuthToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUserName(), authenticationRequest.getPassword())
            );
        } catch (BadCredentialsException badCredentialsException) {
            throw new Exception("Incorrect Username or Password", badCredentialsException);
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUserName());

        final String jwt = jwtUtil.generateToken(userDetails);

        return new AuthenticationResponse(jwt);
    }

    @PostMapping("/validate")
    public ResponseEntity validateToken(@RequestBody AuthenticationResponse jwt) {
        System.out.println(jwt.getJwt());
        Boolean valid = true;
        String userName = jwtUtil.extractUsername(jwt.getJwt());
        final UserDetails userDetails = userDetailsService.loadUserByUsername(userName);
        valid = jwtUtil.validateToken(jwt.getJwt(), userDetails);
        return ResponseEntity.ok(valid);
    }
}
